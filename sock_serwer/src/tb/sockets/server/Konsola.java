package tb.sockets.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;


/* 
 * Pomys� na gre: 
 * Wisielec
 * 
 * Serwer przysy�a ilo�� liter klientowi, oraz kr�tki opis tego s�owa
 * Klient zaczyna z mo�liwoscia uzyskania max pkt = ilosci liter w slowie
 * Klient bedzie mial mozliwosc odgadnieca odrazu calego slowa, w razie blednego odgadniecia - polowa pkt za cale slowo 
 * Klient moze zgadywac litere, jesli trafi, nie traci punktow, jak zle trafi, traci 1/5 - 1/6 punkt�w
 * Ilo�� prob bedzie zalezec od dlugosci s�owa
 * Klient bedzie mial 5-6 pr�b odgadniecia
 * Gdy klient bedzie mial  <= 0 ilosc punktow gra sie konczy, odslaniajac klientowi slowo ktore mial odgadnac
 * Chyba trzeba grafike dodac :/
 * 
 */
public class Konsola {

	public static void main(String[] args) {
		try {
			ServerSocket sSock = new ServerSocket(6666);
			Socket sock = sSock.accept();
			DataInputStream in = new DataInputStream(sock.getInputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(in));
			
			boolean test;
			
			test = (is.read() != 0); //definicja true
			
			
			System.out.println("Przeczytano z gniazdka: " + test);

			is.close();
			in.close();
			sock.close();
			sSock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
